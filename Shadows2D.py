from shapely.geometry import Polygon, LineString
from shapely.ops import unary_union
from math import sin, cos, radians
import pygame
pygame.init()


def draw_lidar_map(sensor_pos_, obstacles_, angle_range, num_rays_):
    def line_intersection(p1, p2, q1, q2):
        det_ = (p2[0] - p1[0]) * (q2[1] - q1[1]) - (p2[1] - p1[1]) * (q2[0] - q1[0])
        if det_ == 0:
            return None

        lambda_numerator = (q2[1] - q1[1]) * (q2[0] - p1[0]) + (q1[0] - q2[0]) * (q2[1] - p1[1])
        mu_numerator = (p1[1] - p2[1]) * (q2[0] - p1[0]) + (p2[0] - p1[0]) * (q2[1] - p1[1])
        lambda_param = lambda_numerator / det_
        mu_param = mu_numerator / det_

        if 0 <= lambda_param <= 1 and 0 <= mu_param <= 1:
            intersection_ = (p1[0] + lambda_param * (p2[0] - p1[0]), p1[1] + lambda_param * (p2[1] - p1[1]))
            return intersection_
        else:
            return None

    def cast_ray(origin, angle_, obstacles__):
        ray_length = 1000  # Arbitrary large value
        ray_end = (origin[0] + ray_length * cos(angle_), origin[1] + ray_length * sin(angle_))

        closest_intersection = None
        min_distance = float('inf')

        for obstacle in obstacles__:
            for j in range(len(obstacle)):
                p1 = obstacle[j]
                p2 = obstacle[(j + 1) % len(obstacle)]

                intersection_ = line_intersection(origin, ray_end, p1, p2)

                if intersection_ is not None:
                    distance = (origin[0] - intersection_[0]) ** 2 + (origin[1] - intersection_[1]) ** 2
                    if distance < min_distance:
                        closest_intersection = intersection_
                        min_distance = distance

        return closest_intersection
    out = []
    for i in range(num_rays_):
        angle = radians(sensor_pos_[2] - angle_range / 2 + (angle_range * i / num_rays_))
        intersection = cast_ray(sensor_pos_[:2], angle, obstacles_)

        if intersection is not None:
            out.append(intersection)
    return out


def get_shadows(obstacles: list, light_sources: list or tuple, rays=600):
    """
    Draws shadows in spaces behind obstacles relative to sensor_coord.

    View point large distances from shadow edges can cause lower shadow quality,
    you can increase rays to combat this, be careful as large amounts of rays > 600
    can cause performance issues.

    :param obstacles: Things to block light (create shadows behind relative to view_coord)
     e.g. [((0, 0), (0, 10), (10, 10), (10, 0))].
    :param light_sources: A list of coordinates for each light source
    :param rays: The number of rays drawn, more rays increases shadow quality.
    :raises ValueError: If the length of coordinates per polygon is less than two.
    :returns shadows, view: Both are lists of shapely Polygons, shadows is the area not covered by light while view is
    the area covered by light.
    """

    screen_width_, screen_height_ = pygame.display.get_window_size()
    sensor_angle = pygame.time.get_ticks() % 1
    s = Polygon([(0, 0), (screen_width_, 0), (screen_width_, screen_height_), (0, screen_height_)])
    polygons_to_combine = []
    for coord in light_sources:
        sensor_pos = (coord[0], coord[1], sensor_angle)
        det = Polygon(draw_lidar_map(sensor_pos, obstacles, 360, rays))
        polygons_to_combine.append(det)

    combined_polygons = unary_union(polygons_to_combine)
    s = s.difference(combined_polygons)

    if isinstance(s, Polygon):
        s = [s]
    elif isinstance(s, LineString):
        s = [s]
    else:
        s = list(s.geoms)  # Convert MultiPolygon to list
    shadows = s
    if isinstance(combined_polygons, Polygon):
        combined_polygons = [combined_polygons]
    elif isinstance(combined_polygons, LineString):
        combined_polygons = [combined_polygons]
    else:
        combined_polygons = list(combined_polygons.geoms)  # Convert MultiPolygon to list
    view = combined_polygons
    return shadows, view


def get_lidar_map(sensor_pos: list, obstacles: list, angle_range=360, num_rays=300):
    sensor_angle = pygame.time.get_ticks() % 1
    sensor_pos = (sensor_pos[0], sensor_pos[1], sensor_angle)
    return draw_lidar_map(sensor_pos, obstacles, angle_range, num_rays)


def draw_shadows(surface, obstacles: list, light_sources: list or tuple, debug=False, rays=600,
                 shadow_colour=(43, 43, 43)):
    """
    Draws shadows in spaces behind obstacles relative to sensor_coord.

    View point large distances from shadow edges can cause lower shadow quality,
    you can increase rays to combat this, be careful as large amounts of rays > 600
    can cause performance issues.

    :param surface: The surface to draw the shadows on e.g. screen.
    :param obstacles: Things to block light (create shadows behind relative to view_coord)
     e.g. [((0, 0), (0, 10), (10, 10), (10, 0))].
    :param light_sources: A list of coordinates for each light source
    :param debug: Shows positions of view_coord, intersection points, rays drawn.
    :param rays: The number of rays drawn, more rays increases shadow quality.
    :param shadow_colour: The colour of the shadow e.g. (43, 43, 43), Although transparency doesn't throw an error,
    for some reason transparency doesn't change it.
    :raises ValueError: If the length of coordinates per polygon is less than two
    """

    screen_width_, screen_height_ = pygame.display.get_window_size()
    sensor_angle = pygame.time.get_ticks() % 1
    s = Polygon([(0, 0), (screen_width_, 0), (screen_width_, screen_height_), (0, screen_height_)])
    det = None
    polygons_to_combine = []
    for coord in light_sources:
        sensor_pos = (coord[0], coord[1], sensor_angle)
        det = Polygon(draw_lidar_map(sensor_pos, obstacles, 360, rays))
        polygons_to_combine.append(det)

    combined_polygons = unary_union(polygons_to_combine)
    s = s.difference(combined_polygons)

    if isinstance(s, Polygon):
        s = [s]
    elif isinstance(s, LineString):
        s = [s]
    else:
        s = list(s.geoms)  # Convert MultiPolygon to list

    for shape in s:
        if isinstance(shape, Polygon):
            result_polygon_coords = list(shape.exterior.coords)
            pygame.draw.polygon(surface, shadow_colour, result_polygon_coords)
        elif isinstance(shape, LineString):
            result_linestring_coords = list(shape.coords)
            pygame.draw.lines(surface, shadow_colour, False, result_linestring_coords)
    if debug:
        for point in det.exterior.coords:
            pygame.draw.circle(surface, (0, 255, 0), point, 1, 1)
