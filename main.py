import os
import pygame
import keyboard
import subprocess
import configparser
from queue import Queue
from random import randint
from time import sleep, time
from Pathfinding2D import CreatePathObject
from Shadows2D import draw_shadows, get_shadows
from shapely.geometry import Polygon, Point, LineString
pygame.init()
screen_width, screen_height = 800, 600
screen_dim = screen_width, screen_height
screen = pygame.display.set_mode((screen_width, screen_height), pygame.HWSURFACE | pygame.DOUBLEBUF)
screen.fill((255, 255, 255))
screen_name = 'Something I Guess'
pygame.display.set_caption(screen_name)
keybinds = []
clock = pygame.time.Clock()
# Colours
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
LIGHTGREY = (200, 200, 200)
RED = (255, 0, 0)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)
GOLD = (212, 175, 55)
GREY = (100, 100, 100)
BG = (43, 43, 43)
# Main Variables
active_inputs = []
pressed = False
press_check = False
queue = Queue()
sleep(0.1)
script_dir = os.path.dirname(os.path.abspath(__file__))


def round_to(in_, decimal=1):
    in_ = str(in_)
    decimal_index = None
    out = ''
    for _ in range(len(in_)):
        if '.' in in_[_]:
            decimal_index = _
            out = out + in_[_]
        elif decimal_index is None:
            out = out + in_[_]
        elif _ <= decimal_index + decimal:
            out = out + in_[_]
    return out


def find_high_low(var):
    high = remove_decimal(var[0])
    low = remove_decimal(var[0])
    for _ in range(len(var)):
        z_ = remove_decimal(var[_])
        if z_ > high:
            high = z_
        elif z_ < low:
            low = z_
    return high, low


def remove_decimal(in_):
    in_ = str(in_)
    out_ = ''
    for _ in range(len(in_)):
        if '.' not in in_[_]:
            out_ = out_ + in_[_]
        else:
            break
    return int(out_)


def close_to(var, var2, thresh):
    re = False
    if thresh > (var - var2) > -thresh:
        re = True
    return re


def find_and_filter_out(in_, rejected):
    in_ = str(in_)
    out_ = ''
    for _ in range(len(in_)):
        if rejected not in in_[_]:
            out_ = out_ + in_[_]
    return out_


def round_10(round_):
    if round_ > 10 or round_ < -10:
        round_ = str(remove_decimal(round_))
        if int(round_[-1]) > 4:
            mid = str(int(round_[-2]) + 1)
        else:
            mid = str((round_[-2]))
        for _ in range(2):
            round_ = remove_text(round_, len(round_) - 1)
        round_ = float(str(round_) + mid + '0')
    elif 10 > round_ > 0:
        round_ = str(remove_decimal(round_))
        if int(round_[-1]) > 4:
            round_ = '10'
        else:
            round_ = '0'
    elif -10 < round_ < 0:
        round_ = str(remove_decimal(round_))
        if int(round_[-1]) > 4:
            round_ = '-10'
        else:
            round_ = '0'
    else:
        if round_ < 0:
            round_ = '-' + str(remove_decimal(round_))
        else:
            round_ = str(remove_decimal(round_))
    return round_


def remove_text(text_, point):
    output = ''
    for _ in range(len(text_)):
        if _ != point:
            output = output + text_[_]
    return output


def number_converter(num_input):
    """
    Removes all characters except numbers and periods
    :param num_input: String to convert
    :return: Number
    """
    num_input = str(num_input)
    num_len = len(num_input)
    allowed_chars = '1234567890.'
    chars_length = len(allowed_chars)
    index = 0
    output = ''
    for _ in range(num_len):
        indexed = num_input[index]
        index_2 = 0
        for i in range(chars_length):
            second_indexed = allowed_chars[index_2]
            if indexed in second_indexed:
                output = output + indexed
            index_2 += 1
        index += 1
    return output


def test_for(test, match):
    return str(match) in str(test)


def replace_index(string_or_list, replacement, index, is_list):
    in_ = string_or_list
    if is_list:
        out = []
        for _ in range(len(in_)):
            if _ == index:
                out = out + [replacement]
            else:
                out = out + [in_[_]]
    else:
        out = ''
        for _ in range(len(in_)):
            if _ == index:
                out = out + replacement
            else:
                out = out + in_[_]
    return out


def replace_instance(string_or_list, replacement, is_list, target):
    in_ = string_or_list
    if is_list:
        out = []
        for _ in range(len(in_)):
            out2 = ''
            for i in range(len(in_[_])):
                if target in in_[_][i]:
                    out2 = out2 + replacement
                else:
                    out2 = out2 + in_[_][i]
            out = out + [out2]
    else:
        out = ''
        for _ in range(len(in_)):
            if target in in_[_]:
                out = out + replacement
            else:
                out = out + in_[_]
    return out


def swap_chars(string_, rep_a, rep_b):
    out = ''
    for _ in range(len(string_)):
        if rep_a in string_[_]:
            out = out + rep_b
        elif rep_b in string_[_]:
            out = out + rep_a
        else:
            out = out + string_[_]
    return out


def text_filter(text_input, allowed_chars):
    """
    Removes all characters that aren't specified as allowed
    :param text_input: The text to be filtered
    :param allowed_chars: The characters allowed in the text
    :return: The filtered text
    """
    text_input = str.lower(str(text_input))
    text_length = len(text_input)
    chars_length = len(allowed_chars)
    index = 0
    output = ''
    for _ in range(text_length):
        indexed = text_input[index]
        index_2 = 0
        for i in range(chars_length):
            second_indexed = allowed_chars[index_2]
            if indexed in second_indexed:
                output = output + indexed
            index_2 += 1
        index += 1
    return output


def extract_decimal(in_):
    ints = ''
    dec_found = False
    dec_count = 2
    for _ in range(len(str(in_))):
        if str(in_)[_] == '.':
            dec_found = True
        elif not dec_found:
            ints = ints + str(in_)[_]
        else:
            dec_count += 1
    ints = int(ints)
    in_ = in_ - ints
    out_ = ''
    for _ in range(dec_count):
        out_ = out_ + str(in_)[_]
    return float(out_)


def extract_setting(content, typ):
    # For text files
    data = ''
    extract = False
    for _ in range(len(content)):
        if extract:
            data = data + content[_]
        elif content[_] == ':':
            extract = True
    if typ == 'int':
        try:
            data = float(data)
        except ValueError:
            data = None
    elif typ == 'bool':
        if 'true' in data:
            data = True
        else:
            data = False
    return data


def get_settings(settings, path='data.ini'):
    """
    Extracts settings from the relative path and formats them into an immediately usable format.
    :param settings: Format - [(default setting, ('setting section', 'setting name'), 'type of variable (int/bool)')]
    :param path: Path to settings (relative)
    """
    path = os.path.join(script_dir, path)
    config = configparser.ConfigParser()
    config.read(path)
    data = []
    for _ in range(len(settings)):
        setting = settings[_][0]
        if 'int' in settings[_][2]:
            try:
                setting = config.getfloat(settings[_][1][0], settings[_][1][1])
            except configparser.NoSectionError:
                notification.add(f'File Error in {path}', 'Missing Setting Section')
            except configparser.NoOptionError:
                notification.add(f'File Error in {path}', 'Missing Setting')
            except configparser.Error:
                notification.add(f'File Error in {path}', 'An Unknown Error has occurred')
            except ValueError:
                notification.add('Value Error', f'Invalid Setting Value,'
                                                f' {config.get(settings[_][1][0], settings[_][1][1])}')
        elif 'bool' in settings[_][2]:
            try:
                setting = config.get(settings[_][1][0], settings[_][1][1])
                if 'true' in str.lower(str(setting)):
                    setting = True
                else:
                    setting = False
            except configparser.NoSectionError:
                notification.add(f'File Error in {path}', 'Missing Setting Section')
            except configparser.NoOptionError:
                notification.add(f'File Error in {path}', 'Missing Setting')
            except configparser.Error:
                notification.add(f'File Error in {path}', 'An Unknown Error has occurred')
            except ValueError:
                notification.add('Value Error', 'Invalid Setting Value,'
                                                f' {config.get(settings[_][1][0], settings[_][1][1])}')
        elif 'str' in settings[_][2]:
            try:
                setting = str(config.get(settings[_][1][0], settings[_][1][1]))
            except configparser.NoSectionError:
                notification.add(f'File Error in {path}', 'Missing Setting Section')
            except configparser.NoOptionError:
                notification.add(f'File Error in {path}', 'Missing Setting')
            except configparser.Error:
                notification.add(f'File Error in {path}', 'An Unknown Error has occurred')
            except ValueError:
                notification.add('Value Error', 'Invalid Setting Value,'
                                                f' {config.get(settings[_][1][0], settings[_][1][1])}')
        data = data + [setting]

    return data


def set_settings(settings, path='data.ini'):
    """
    Extracts settings from the relative path and formats them into an immediately usable format.
    :param settings: Format - [(setting, ('setting section', 'setting name'))]
    :param path: Path to settings (relative)
    """
    path = os.path.join(script_dir, path)
    config = configparser.ConfigParser()
    config.read(path)
    for _ in range(len(settings)):
        section = settings[_][1][0]
        name = settings[_][1][1]
        setting = settings[_][0]
        try:
            config.set(section, name, str(setting))
        except configparser.NoSectionError:
            notification.add(f'File Error in {path}', 'Missing Setting Section', tp='click')
        except configparser.NoOptionError:
            notification.add(f'File Error in {path}', 'Missing Setting', tp='click')
        except configparser.Error:
            notification.add(f'File Error in {path}', 'An Unknown Error has occurred', tp='click')

    with open(path, 'w') as f:
        config.write(f)


def toggle(in_):
    if in_:
        out = False
    else:
        out = True
    return out


def if_mouse_clicking():
    global pressed, press_check
    if pressed:
        if not press_check:
            pressed = True
            press_check = True
        else:
            pressed = False
    else:
        pressed = False
        press_check = False
    return pressed and not press_check


def get_change(inputs):
    return inputs[-1] - inputs[0]


def ave(items):
    try:
        if len(items) > 1:
            al = 0
            __ = 0
            for _ in range(len(items)):
                __ = _
                try:
                    al += float(items[_])
                except ValueError:
                    pass
            out = al / __
        else:
            out = items[0]
    except TypeError:
        out = items
    return out


def get_gpu_usage():
    try:
        output = subprocess.check_output(["nvidia-smi", "--query-gpu=utilization.gpu", "--format=csv"]).decode("utf-8")
        gpu_usage = [int(x.split()[0]) for x in output.strip().split("\n")[1:]]
        return gpu_usage
    except Exception as e:
        print("Error getting GPU usage:", e)
        return []


class Throttling:
    def __init__(self, retriever, sender=None, timing=1, start_=None):
        self.r = retriever
        self.s = sender
        self.t = timing
        self.n = time()
        self.store = start_

    def update(self):
        t = time() - self.n
        if t > self.t:
            d = self.r()
            if self.s is not None:
                self.s(d)
            self.store = d
            self.n = time()


class TargetController:
    """
    Slowly increase in speed towards the target before slowing down when reaching the target
    :param target: What to go to
    :param transition_time: How long to reach target
    """

    def __init__(self, target, transition_time=1.0):
        self.target = target
        self.transition_time = transition_time
        self.output = 0.0
        self.t = 0.0
        self.velocity = 0.0

    def change_target(self, target):
        self.target = remove_decimal(target)
        self.t = 0.0
        self.velocity = 0.0

    def update(self, elapsed_time):
        self.t += elapsed_time
        if self.t > self.transition_time:
            self.output = self.target
            self.velocity = 0.0
            return

        t = self.t / self.transition_time
        self.output = self.target * t + (1 - t) * self.output
        self.velocity = (self.target - self.output) / elapsed_time


class NumberAverager:
    def __init__(self, time_last, process_on_average=True):
        self.numbers = []
        self.t = time_last
        self.poa = process_on_average

    def add_number(self, num):
        self.numbers.append((num, time()))

    def get_average(self):
        if self.poa:
            self.remove_old_numbers()
        if not self.numbers:
            return 0
        return sum(num[0] for num in self.numbers) / len(self.numbers)

    def remove_old_numbers(self):
        now = time()
        self.numbers = [(num, timestamp) for (num, timestamp) in self.numbers if now - timestamp <= self.t]


def draw_lines(x, y, text_, center=False, size=20, bg_colour=WHITE, fill=False, font_colour=LIGHTGREY):
    size = int(size)
    font = pygame.font.SysFont('Arial', size=size)
    for line in text_:
        text = font.render(str(line), True, font_colour)
        text_rect = text.get_rect()
        if center:
            text_rect.center = (x, y)
        else:
            text_rect.x = x
            text_rect.y = y
        if fill:
            bg_surface = pygame.Surface(text.get_size())
            bg_surface.fill(bg_colour)
            bg_surface.blit(text, (text_rect.x, text_rect.y))
            screen.blit(bg_surface, (text_rect.x, text_rect.y))
        screen.blit(text, text_rect)
        y += size * 1.3


def draw_content(x, y, heading, body, text_length, center=False, heading_size=25, body_size=20, bg_colour=WHITE,
                 fill=False, font_colour=LIGHTGREY):
    size = int(heading_size)
    font = pygame.font.SysFont('Arial', size=size)
    text = font.render(str(heading), True, font_colour)
    text_rect = text.get_rect()
    if center:
        text_rect.center = (x, y)
    else:
        text_rect.x = x
        text_rect.y = y
    if fill:
        bg_surface = pygame.Surface(text.get_size())
        bg_surface.fill(bg_colour)
        bg_surface.blit(text, (text_rect.x, text_rect.y))
        screen.blit(bg_surface, (text_rect.x, text_rect.y))
    screen.blit(text, text_rect)
    body = format_paragraph(body, text_length)
    y += heading_size * 1.3
    draw_lines(x, y, body, center, body_size, bg_colour, fill, font_colour)
    return text.get_size()


def draw_text(x, y, text='', center=False, size=20, bg_colour=WHITE, fill=False, font_colour=LIGHTGREY):
    size = int(size)
    font = pygame.font.SysFont('Arial', size=size)
    text = font.render(str(text), True, font_colour)
    text_rect = text.get_rect()
    if center:
        text_rect.center = (x, y)
    else:
        text_rect.x = x
        text_rect.y = y
    if fill:
        bg_surface = pygame.Surface(text.get_size())
        bg_surface.fill(bg_colour)
        bg_surface.blit(text, (text_rect.x, text_rect.y))
        screen.blit(bg_surface, (text_rect.x, text_rect.y))
    screen.blit(text, text_rect)
    return text.get_size()


def draw_line(start_x, start_y, end_x, end_y, colour=LIGHTGREY, screen_=screen):
    pygame.draw.line(screen_, colour, (start_x, start_y), (end_x, end_y))


def draw_rect(x, y, width, height, fill=False, colour=BG):
    if fill:
        border = 0
    else:
        border = 1
    pygame.draw.rect(screen, colour, (x, y, width, height), border)


def draw_circle(coord: list, radius: float, fill=False, colour=BG):
    if fill:
        border = 0
    else:
        border = 1
    pygame.draw.circle(screen, colour, coord, radius, border)


def draw_arc(x, y, width, height, start_angle, end_angle, fill=False, colour=BLACK):
    if fill:
        border = 0
    else:
        border = 1
    pygame.draw.arc(screen, colour, (x, y, width, height), start_angle, end_angle, border)


def get_mouse():
    x, y = pygame.mouse.get_pos()
    return x, y


def if_pressed():
    global pressed
    pressed = pygame.mouse.get_pressed()[0] and not active_inputs_on()
    return pressed and not press_check


def draw_button(x, y, text, size=20, bg_colour=(43, 43, 43), font_colour=LIGHTGREY, center=False, fill=False,
                border=True):
    text = str(text)
    font = pygame.font.SysFont('Arial', size=size)
    tw, th = font.size(text)
    if center:
        x -= tw / 2
    if fill:
        draw_rect(x - 3, y, tw + 6, th + 2, colour=(43, 43, 43), fill=True)
    if border:
        draw_rect(x - 3, y, tw + 6, th + 2, colour=LIGHTGREY, fill=False)
    draw_text(x, y, text, size=size, fill=False, bg_colour=bg_colour, font_colour=font_colour)
    x -= 3
    tw += 6
    th += 2
    mx, my = get_mouse()
    return x < mx < (x + tw) and y < my < (y + size) and if_pressed()


def is_pressing(x, y, width, height):
    mx, my = get_mouse()
    return x < mx < (x + width) and y < my < (y + height) and if_pressed()


def is_pressing_constantly(x, y, width, height):
    mx, my = get_mouse()
    return x < mx < (x + width) and y < my < (y + height) and pygame.mouse.get_pressed()[0]


def format_paragraph(paragraph, max_chars_per_line):
    words = paragraph.split()
    lines = []
    current_line = ""
    for word in words:
        if len(current_line) + len(word) + 1 <= max_chars_per_line:
            current_line += word + " "
        else:
            lines.append(current_line.strip())
            while len(word) > max_chars_per_line:
                lines.append(word[:max_chars_per_line - 1] + "-")
                word = word[max_chars_per_line - 1:]
            current_line = word + " "
    if current_line:
        lines.append(current_line.strip())
    return lines


class CreateNotificationCenter:
    def __init__(self, x, y, size=20, time_last=5, default='time'):
        self.x = x + 2
        self.y = y
        self.s = size
        self.tl = time_last
        self.notes = []
        self.w = 0
        self.default = default
        self.types = ['click', 'time']

    def find(self, title, changes=None):
        found = False
        for _ in range(len(self.notes)):
            if str.lower(title) == str.lower(self.notes[_][0]):
                if changes is not None:
                    for j in range(len(changes)):
                        self.notes[_][changes[j][0]] = changes[j][1]
                found = True
        self.get_width()
        return found

    def draw(self):
        self.remove()
        self.get_width()
        for _ in range(len(self.notes)):
            try:
                data = self.notes[_]
            except IndexError:
                data = self.notes[0]
            y = _ * (self.s * 2.7) + self.y
            draw_rect(self.x - 2, y, self.w + 4, self.s * 2.7 - 1, fill=True, colour=(43, 43, 43))
            draw_text(self.x, y, data[0], size=self.s)
            draw_text(self.x, y + self.s * 1.3, data[1], size=self.s / 1.5)
            draw_rect(self.x - 2, y, self.w + 4, self.s * 2.7 - 1)
            if data[5] == 'time':
                if time() - data[2] > 0.021:
                    draw_rect(self.x - 2, y + self.s * 2.2, ((time() - data[2]) / data[4]) * self.w + 4, self.s / 2 - 1,
                              fill=True, colour=LIGHTGREY)
            elif data[5] == 'click':
                draw_rect(self.x - 2, y + self.s * 2.2, self.w + 4, self.s / 2 - 1,
                          fill=True, colour=GREY)

                if is_pressing(self.x - 2, y + self.s * 2.2, self.w + 4, self.s / 2 - 1):
                    self.remove_spec(self.notes[_][0])

                draw_text(self.x + self.w / 2, y + self.s * 2.2 + (self.s / 4) - 1, 'Continue', center=True,
                          size=self.s / 2)

            draw_rect(self.x - 2, y + self.s * 2.2, self.w + 4, self.s / 2 - 1)

    def remove(self):
        n = []
        for _ in range(len(self.notes)):
            if self.notes[_][5] == 'time':
                if (time() - self.notes[_][2]) < self.notes[_][4]:
                    n.append(self.notes[_])
            else:
                n.append(self.notes[_])
        self.notes = n

    def remove_spec(self, name):
        n = []
        for _ in range(len(self.notes)):
            if self.notes[_][0] != name:
                n.append(self.notes[_])
        self.notes = n

    def get_width(self):
        if len(self.notes) > 0:
            widths = [draw_text(-1000, -1000, 'Continue', size=self.s / 2)[0] + 4]
            for _ in range(len(self.notes)):
                widths.append(self.notes[_][3])
            self.w = max(widths)

    def add(self, title, description, time_last=None, tp='time'):
        w1 = draw_text(-1000, -1000, title, size=self.s)[0]
        w2 = draw_text(-1000, -1000, description, size=self.s / 1.5)[0]
        if w1 > w2:
            w = w1
        else:
            w = w2
        if time_last is None:
            tl = self.tl
        else:
            tl = time_last
        if tp not in self.types:
            notification.add('Notification Error', f"'{title}' has unknown type '{tp}'", tp='click')
            tp = self.default
        if not self.find(title, [(2, time())]):
            self.notes.append([title, description, float(time()), w, tl, tp])


class CreateLoadingScreen:
    def __init__(self, items):
        self.mi = items
        self.i = 0
        self.c = 0

    def update(self, data=''):
        while self.c < self.i:
            screen.fill((43, 43, 43))
            if self.c < self.i:
                self.c += 0.1
            draw_text(screen_width / 2, screen_height / 3, 'Loading...', True, size=30)
            draw_text(screen_width / 2, screen_height / 2, data, True, size=25)
            draw_rect(25, screen_width / 2 - 10, screen_width - 50, 20, colour=LIGHTGREY)
            draw_rect(25, screen_width / 2 - 10, (screen_width - 50) * (self.c / self.mi), 20, colour=LIGHTGREY,
                      fill=True)
            pygame.display.flip()
            sleep(0.005)
        self.i += 1


def get_eta(pin_len, engine):
    collected = get_settings([('-', ('c', f'collected{engine}'), 'str')])[0]
    if pin_len is None or collected[0] == '-':
        return None
    else:
        pin_len = str(pin_len)
        c = []
        bm = -1
        t = ''
        for _ in range(len(collected)):
            if _ > bm:
                if collected[_] == '.':
                    bm = _
                    c.append(t)
                    t = ''
                else:
                    t = t + collected[_]
        if pin_len in c:
            collected = get_settings([('0', (f's{engine}', f'l{pin_len}'), 'str')])[0]
            c = []
            bm = -1
            t = ''
            for _ in range(len(collected)):
                if _ > bm:
                    if collected[_] == ',':
                        bm = _
                        c.append(float(t))
                        t = ''
                    else:
                        t = t + collected[_]
            t = 0
            for _ in range(len(c)):
                t += c[_]
            try:
                out = t / len(c)
            except ZeroDivisionError:
                return None
            return convert_seconds(out)
        else:
            return None


def remove_all_settings(section, replace_section=True, path='data.ini'):
    path = os.path.join(script_dir, path)
    config = configparser.ConfigParser()
    config.read(path)
    config.remove_section(section)
    if replace_section:
        config.add_section(section)
    with open(path, 'w') as f:
        config.write(f)


def draw_are_you_sure():
    x = draw_text(-1000, -1000, 'Are you Sure?', center=True, size=30)[0]
    det = False
    while True:
        draw_rect(screen_width / 2 - x / 2, screen_height / 2 - 10, x, 65, fill=True, colour=(43, 43, 43))
        draw_rect(screen_width / 2 - x / 2, screen_height / 2 - 10, x, 65)
        draw_text(screen_width / 2, screen_height / 2, 'Are you Sure?', center=True, size=30)
        if draw_button(screen_width / 2 - 50, screen_height / 2 + 20, 'No', size=25):
            break
        if draw_button(screen_width / 2 + 10, screen_height / 2 + 20, 'Yes', size=25):
            det = True
            break
        pygame.display.flip()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
    return det


def add_eta(pin_len, eta, engine):
    collected = get_settings([('', ('c', f'collected{engine}'), 'str')])[0]
    if pin_len is None:
        return None
    else:
        pin_len = str(pin_len)
        c = []
        bm = -1
        t = ''
        for _ in range(len(collected)):
            if _ > bm:
                if collected[_] == '.':
                    bm = _
                    c.append(t)
                    t = ''
                else:
                    t = t + collected[_]
        al = 'l' + pin_len
        eta = str(eta)
        if pin_len in c:
            oeta = get_settings([(0, (f's{engine}', al), 'str')])[0]
            set_settings([(oeta + eta + ',', (f's{engine}', al))])
        else:
            set_settings([(eta + ',', (f's{engine}', al))])
            if collected[0] == '-':
                set_settings([(pin_len + '.', ('c', f'collected{engine}'))])
            else:
                set_settings([(collected + pin_len + '.', ('c', f'collected{engine}'))])


def read_file(relative_path='changelog.txt'):
    path = os.path.join(script_dir, relative_path)
    with open(path, 'r') as file:
        data = file.readlines()
        file.close()
    return data


class CreateScrollingText:
    def __init__(self, x, y, width, height, text, scroll_width=10, size=20):
        self.x = x
        self.y = y
        self.w = width
        self.sw = scroll_width
        self.s = size
        self.sc = 0
        self.h = height
        self.ot = text
        text_ = 'h'
        for _ in range(999):
            text_ = 'a' + text_
        tl = len(text_)
        tw = draw_text(-1000, -1000, text_, size=size)[0]
        if tw > width:
            while tw > width:
                t = format_paragraph(text_, tl)[0]
                if len(t) == 0:
                    t = format_paragraph(text_, tl)[1]
                tw = draw_text(-1000, -1000, t, size=size)[0]
                tl -= 1
        self.tl = tl
        texts = []
        for _ in range(len(text)):
            string_ = format_paragraph(text[_], tl)
            for j in range(len(string_)):
                texts.append(string_[j])
        self.t = texts
        self.p = False
        self.mp = 0

    def change_text(self, text):
        texts = []
        for _ in range(len(text)):
            if len(text[_]) > 1:
                string_ = format_paragraph(text[_], self.tl)
                for j in range(len(string_)):
                    texts.append(string_[j])
            else:
                texts.append(' ')
        self.t = texts

    def draw(self):
        draw_rect(self.x - 3, 0, self.w + 6, screen_height, colour=(43, 43, 43), fill=True)
        draw_lines(self.x, self.y - (self.s * 1.4 * len(self.t)) * self.sc, self.t, center=False)
        draw_rect(self.x - 3, self.h + self.y, self.w + 6 + self.sw, screen_height - self.y - self.h,
                  colour=(43, 43, 43), fill=True)
        draw_rect(self.x - 3, 0, self.w + 6, self.y, colour=(43, 43, 43), fill=True)
        draw_rect(self.x - 3, self.y, self.w + 6 + self.sw, self.h, colour=LIGHTGREY)
        if is_pressing_constantly(self.x, self.y, self.w + self.sw, self.h) or self.p:
            self.mp = get_mouse()[1]
            self.p = True
        else:
            if keyboard.is_pressed('up'):
                self.mp -= 1
            elif keyboard.is_pressed('down'):
                self.mp += 1
        sc = self.mp - 10
        if sc < self.y:
            sc = self.y
        elif sc > self.y + self.h:
            sc = self.y + self.h
        self.sc = (sc - self.y) / self.h
        if not pygame.mouse.get_pressed()[0]:
            self.p = False
        if self.sc > 0.94:
            sc = 0.945
        else:
            sc = self.sc
        draw_rect(self.x + self.w + 3, self.y + (self.h * sc) + 1, self.sw - 2, 20, fill=True, colour=GREY)


def list_true(bools):
    index = 0
    out = False
    for _ in range(len(bools)):
        if bools[_]:
            out = True
            index = _
    return out, index


class CreateMenu:
    def __init__(self, x, y, name='Menu', size=20):
        self.x = x
        self.y = y
        self.n = name
        self.b = []
        self.o = []
        self.s = size
        self.w = 1
        self.ws = []
        self.wn = draw_text(-1000, -1000, name, size=self.s)[0]
        self.open = True
        self.t = 0
        self.t2 = self.wn + 7
        self.h = 1

    def find(self, name):
        found = False
        index = 0
        for _ in range(len(self.b)):
            if name == self.b[_][0]:
                found = True
                index = _
        return found, index

    def add(self, name):
        if self.find(name)[0]:
            self.remove(name)
        self.b.append((name, str.lower(name)))
        self.ws.append(draw_text(-1000, -1000, name, size=int(round(self.s * 0.75)))[0])
        self.o.append(False)
        self.get_width()
        pygame.mouse.get_rel()

    def remove(self, name):
        n = []
        for _ in range(len(self.b)):
            if name not in self.b[_]:
                n.append(self.b[_])
        self.b = n

    def get_width(self):
        if len(self.ws) > 0:
            self.w = max(self.ws + [self.wn]) + 6

    def replace(self, replaced, replacer):
        found, _ = self.find(replaced)
        if found:
            self.ws[_] = draw_text(-1000, -1000, replacer, size=int(round(self.s * 0.75)))[0]
            self.b[_] = (str.title(replacer), str.lower(replacer))
            self.get_width()
        else:
            notification.add('Menu Failure', f'{self.n} Failed to Replace Button', tp='click')

    def draw(self):
        self.get_width()
        x = self.x
        y = self.y
        s = int(round(self.s * 0.75))
        self.h = 4 * self.t + 1 + self.s * self.t * (len(self.b) + 1)
        draw_rect(x - 3, y, self.w, 4 * self.t + 1 + self.s * self.t * (len(self.b) + 1), colour=(43, 43, 43),
                  fill=True)
        y += 4 * self.t + 1
        if self.t > 0.01 and self.t2 > self.w - 1:
            for _ in range(len(self.b)):
                y += (self.s - 2) * self.t
                draw_rect(x - 2, y, self.w, self.s + 3, colour=(43, 43, 43), fill=True)
                if self.o[_]:
                    colour = GOLD
                else:
                    colour = LIGHTGREY
                draw_button(x, y, self.b[_][0], size=s, bg_colour=(43, 43, 43), fill=True, border=False,
                            font_colour=colour)
                if is_pressing(x - 2, y, self.w + 4, s) and self.t > 0.99:
                    self.o[_] = toggle(self.o[_])
                if _ + 1 == len(self.b):
                    add = -1
                else:
                    add = 3
                draw_rect(x - 3, y, self.w, self.s + add, colour=LIGHTGREY)
        if self.open and self.t2 > self.w - 1:
            self.t -= (self.t - 1) / 5
        else:
            self.t -= self.t / 5
        if self.t > 0.01 or self.open:
            self.t2 -= (self.t2 - self.w) / 4
        else:
            self.t2 -= (self.t2 - (self.wn + 7)) / 5
        x = self.x
        y = self.y
        draw_rect(x - 2, y, self.w, self.s + 3, colour=(43, 43, 43), fill=True)
        if draw_button(x, y, self.n, size=self.s, fill=False, border=False):
            self.open = toggle(self.open)
        if self.t > 0.01 and self.t2 > self.w - 1:
            draw_rect(self.x - 3, self.y, self.w, self.s + 3, colour=LIGHTGREY)
        else:
            draw_rect(x - 3, y, self.t2, self.s + 3, colour=LIGHTGREY)
        return self.o


class CreateScreenMenu:
    def __init__(self, x, y, default, name='Menu', size=20, reset_if_duplicate=False, replace_key='home'):
        self.x = x
        self.y = y
        self.n = name
        self.b = []
        self.bn = []
        self.o = []
        self.s = size
        self.w = 1
        self.ws = []
        self.wn = draw_text(-1000, -1000, name, size=self.s)[0]
        self.open = False
        self.t = 0
        self.t2 = self.wn + 7
        self.h = 1
        self.output = default
        self.rid = reset_if_duplicate
        self.rk = replace_key

    def find(self, name):
        found = False
        index = 0
        for _ in range(len(self.b)):
            if name == self.b[_][0]:
                found = True
                index = _
        return found, index

    def add(self, name):
        if self.find(name)[0]:
            self.remove(name)
        self.b.append((name, str.lower(name)))
        self.bn.append(name)
        self.ws.append(draw_text(-1000, -1000, name, size=int(round(self.s * 0.75)))[0])
        self.o.append(False)
        self.get_width()

    def find_multiple(self):
        found = False
        for _ in range(len(self.b)):
            for j in range(len(self.b)):
                if _ != j and str.lower(self.b[_][0]) == str.lower(self.b[j][0]):
                    notification.add('Menu Error', 'Menu Duplicate found, reset menu', tp='click')
                    self.output = self.rk
                    self.b = []
                    self.ws = []
                    self.o = []
                    for i in range(len(self.bn)):
                        self.add(self.bn[i])
        return found

    def remove(self, name):
        self.find_multiple()
        n = []
        nb = []
        for _ in range(len(self.b)):
            if name not in self.b[_]:
                n.append(self.b[_])
                nb.append(self.b[_][0])
        self.b = n
        self.bn = nb

    def get_width(self):
        if len(self.ws) > 0:
            self.w = max(self.ws + [self.wn]) + 6

    def replace(self, replaced, replacer):
        found, _ = self.find(replaced)
        if found:
            self.ws[_] = draw_text(-1000, -1000, replacer, size=int(round(self.s * 0.75)))[0]
            self.b[_] = (str.title(replacer), str.lower(replacer))
            self.get_width()
        else:
            notification.add('Menu Failure', f'{self.n} Failed to Replace Button', tp='click')

    def draw(self):
        self.find_multiple()
        self.get_width()
        x = self.x
        y = self.y
        s = int(round(self.s * 0.75))
        self.h = 4 * self.t + 1 + self.s * self.t * (len(self.b) + 1)
        draw_rect(x - 3, y, self.w, 4 * self.t + 1 + self.s * self.t * (len(self.b) + 1), colour=(43, 43, 43),
                  fill=True)
        self.o = []
        y += 4 * self.t + 1
        if self.t > 0.01 and self.t2 > self.w - 1:
            for _ in range(len(self.b)):
                y += (self.s - 2) * self.t
                draw_rect(x - 2, y, self.w, self.s + 3, colour=(43, 43, 43), fill=True)
                self.o.append(draw_button(x, y, self.b[_][0], size=s, bg_colour=(43, 43, 43), fill=True, border=False)
                              and self.t > 0.99)
                if _ + 1 == len(self.b):
                    add = -1
                else:
                    add = 3
                draw_rect(x - 3, y, self.w, self.s + add, colour=LIGHTGREY)
        else:
            for _ in range(len(self.b)):
                self.o.append(False)
        if self.open and self.t2 > self.w - 1:
            self.t -= (self.t - 1) / 5
        else:
            self.t -= self.t / 5
        if self.t > 0.01 or self.open:
            self.t2 -= (self.t2 - self.w) / 4
        else:
            self.t2 -= (self.t2 - (self.wn + 7)) / 5
        x = self.x
        y = self.y
        draw_rect(x - 2, y, self.w, self.s + 3, colour=(43, 43, 43), fill=True)
        if draw_button(x, y, self.n, size=self.s, fill=False, border=False):
            self.open = toggle(self.open)
        if self.t > 0.01 and self.t2 > self.w - 1:
            draw_rect(self.x - 3, self.y, self.w, self.s + 3, colour=LIGHTGREY)
        else:
            draw_rect(x - 3, y, self.t2, self.s + 3, colour=LIGHTGREY)
        if list_true(self.o)[0]:
            self.open = False
            alias_n = self.b[list_true(self.o)[1]][1]
            self.replace(str.title(self.b[list_true(self.o)[1]][1]), str.title(self.output))
            self.output = alias_n
        return self.o


class DefineActiveInput:
    def __init__(self, x, y, title, type_, start, size=20, reset_on_click=False, allow=None, allow_none=False):
        self.x = x
        self.y = y
        self.s = size
        self.n = title
        self.i = start
        self.o = start
        self.on = False
        self.t = type_
        self.roc = reset_on_click
        self.al = allow
        self.aln = allow_none
        self.lock = False
        active_inputs.append(self)

    def update(self, draw=True):
        error = True
        temp = ''
        if self.on and self.lock:
            self.on = False
        if self.t == 'int':
            try:
                if not self.on:
                    self.o = float(self.i)
                temp = float(self.i)
                error = False
            except ValueError:
                error = True
        elif self.t == 'str':
            try:
                temp = str(self.i)
                found = False
                if self.al is not None:
                    temp2 = text_filter(temp, self.al)
                    found = temp2 != temp
                    # draw_lines(get_mouse()[0] + 25, get_mouse()[1], (temp, temp2, self.al, self.o))
                    temp = temp2
                error = False
                if not self.aln and len(temp) == 0 or found:
                    error = True
                if not self.on and not error and not found:
                    self.o = temp
            except ValueError:
                error = True
        elif self.t == 'bool':
            self.i = str.lower(str(self.i))
            if ('true' in self.i and len(self.i) == 4) or ('false' in self.i and len(self.i) == 5):
                error = False
                self.i = str.title(self.i)
                if not self.on:
                    self.o = bool(self.i)
        elif self.t == 'key':
            try:
                self.i = str.title(str(self.i))
                temp = keyboard.is_pressed(str(self.i))
                if not self.on:
                    self.o = str(self.i)
                error = False
            except ValueError:
                error = True
        if self.on and not error:
            colour = (178, 230, 170)
        elif error:
            if self.on:
                colour = (230, 154, 154)
            else:
                colour = (237, 57, 57)
                notification.add(f'{self.n} Invalid', f"{self.n}'s input is invalid")
        else:
            colour = LIGHTGREY
        if temp == float('inf'):
            pass
        if draw:
            if draw_button(self.x, self.y, f'{self.n}: {self.i}', size=self.s, font_colour=colour):
                if self.roc:
                    self.i = ''
                self.on = toggle(self.on)
        if keyboard.is_pressed('alt') and self.on:
            if self.t == 'int':
                self.i = remove_decimal(self.o)
            else:
                self.i = self.o
            self.on = False


def on_press_callback(event):
    if event.event_type == 'down':
        key = str.lower(str(event.name))
        for _ in range(len(active_inputs)):
            if active_inputs[_].on:
                keys = str(active_inputs[_].i)
                if key == 'backspace':
                    keys = keys[:-1]
                elif key == 'space':
                    keys = keys + ' '
                elif key == 'enter':
                    active_inputs[_].on = False
                elif len(key) > 1:
                    pass
                else:
                    if keyboard.is_pressed('shift'):
                        if len(key) == 1:
                            keys = keys + str.upper(key[-1])
                    else:
                        keys = keys + str.lower(key)
                active_inputs[_].i = keys


def active_inputs_on():
    on = False
    for _ in range(len(active_inputs)):
        if active_inputs[_].on:
            on = True
    return on


def convert_seconds(seconds, formatted=True):
    seconds = float(seconds)
    minutes, seconds = divmod(seconds, 60)
    hours, minutes = divmod(minutes, 60)
    days, hours = divmod(hours, 24)
    if formatted:
        return f"{round(days)} days, {round(hours)} hours, {round(minutes)} minutes, {round_to(seconds, 2)} seconds"
    else:
        return round(days), round(hours), round(minutes), round(seconds)


notification = CreateNotificationCenter(5, 350)
keyboard.on_press(on_press_callback)


def draw_at_mouse(lines, fill=True):
    draw_lines(get_mouse()[0] + 20, get_mouse()[1], lines, font_colour=BG, bg_colour=WHITE, fill=fill)


def format_points(data):
    d = []
    t = ''
    for _ in range(len(data)):
        if data[_] == ';':
            t = float(t.split(',')[0]), float(t.split(',')[1])
            d.append(t)
            t = ''
        elif data[_] != '(' and data[_] != ')':
            t = t + data[_]
    return d


def load_save(name):
    path = os.path.join(script_dir, name + '.ini')
    data = get_settings([('(0,0);(10,0);(10,10);', ('gci', 'points'), 'str')], path)
    points = data[0]
    points = format_points(points)
    notification.add(f'{name} loaded', f'Complexity: {len(points)}')
    return points


def draw_poly_g(rect, fill=False, colour=BG):
    if fill:
        border = 0
    else:
        border = 1
    pygame.draw.polygon(screen, colour, rect, border)


class CreateAI:
    def __init__(self, coord: list or tuple, obstacles: list, colour=(0, 255, 0), speed=2, ai='static'):
        """
        :param coord: Starting Coordinates.
        :param ai: static, random, search
        :param colour:
        :param speed:
        """
        self.ai = ai
        self.spawn_coord = coord
        self.x, self.y = coord
        self.coord = coord
        self.wp = obstacles
        self.co = colour
        self.sp = speed
        self.path = []
        self.op = []
        self.path_object = CreatePathObject(obstacles, screen_width, screen_height, 0.2, round(speed))

    def update_obstacles(self, obstacles, speed):
        self.path_object = CreatePathObject(obstacles, screen_width, screen_height, 0.15, round(speed))

    def search(self, search_coords=None):
        if search_coords is not None:
            if len(search_coords) > 0:
                view = get_shadows(self.wp, [self.coord], 200)[1][0]
                index = None
                for _ in range(len(search_coords)):
                    if Polygon(view).intersects(Point(search_coords[_])):
                        index = _
                if index is not None:
                    targ = search_coords[index]
                    self.pathfind(targ)
                else:
                    self.rand_path_find()
            else:
                self.rand_path_find()
        else:
            self.rand_path_find()

    def rand_path_find(self):
        targ = randint(0, screen_width), randint(0, screen_height)
        self.pathfind(targ)

    def path_find_to(self, coordinates):
        err = False
        if len(self.path) > 0:
            x, y = self.path[-1]
            x2, y2 = coordinates
            if not (close_to(x, x2, self.sp + 1) and close_to(y, y2, self.sp + 1)):
                err = True
        else:
            err = True
        if err:
            self.pathfind(coordinates)

    def pathfind(self, target):
        targ = target
        polys = [Polygon(poly) for poly in self.wp]
        line = LineString([self.coord, targ])
        inter = False
        for poly in polys:
            if poly.intersects(line):
                inter = True
        self.path = self.path_object.calculate(self.coord, targ)
        # Faster pathfinding statement isn't being used until self.path_object.calculate is fixed
        # if inter:
        #     self.path = self.path_object.calculate(self.coord, targ)
        # else:
        #     self.path = create_line_segments(self.coord, targ, self.sp)

    def update(self, search_coords=None, auto_reset=True):
        if search_coords is not None:
            view = get_shadows(self.wp, [self.coord], 200)[1][0]
            index = None
            for _ in range(len(search_coords)):
                if Polygon(view).intersects(Point(search_coords[_])):
                    index = _
            if index is None:
                index = False
            elif len(self.path) > 0:
                x, y = search_coords[index]
                x2, y2 = self.path[-1]
                if not (close_to(x, x2, self.sp + 1) and close_to(y, y2, self.sp + 1)):
                    index = True
                self.coord = self.path[0]
                self.x, self.y = self.coord
                p = []
                for _ in range(len(self.path)):
                    if _ > 0:
                        p.append(self.path[_])
                self.path = p
            else:
                index = False
        else:
            index = False
        if len(self.path) > 0 and not index:
            self.coord = self.path[0]
            self.x, self.y = self.coord
            p = []
            for _ in range(len(self.path)):
                if _ > 0:
                    p.append(self.path[_])
            self.path = p
        elif auto_reset:
            if self.ai == 'random':
                self.rand_path_find()
            elif self.ai == 'static':
                pass
            elif self.ai == 'search':
                self.search(search_coords)

    def draw(self, search_coords=None, update=True, debug=False):
        try:
            for _ in range(len(self.path)):
                if _ > 0:
                    if _ % 2 == 0:
                        c = BLUE
                    else:
                        c = GREEN
                    pygame.draw.line(screen, c, self.path[_ - 1], self.path[_])
                else:
                    pygame.draw.line(screen, BLUE, self.coord, self.path[_])
        except TypeError:
            pass
        if debug:
            for _ in range(len(self.wp)):
                pygame.draw.polygon(screen, GREEN, self.wp[_], width=1)
            for _ in range(len(self.op)):
                if _ > 0:
                    pygame.draw.line(screen, RED, self.op[_ - 1], self.op[_])
                else:
                    pygame.draw.line(screen, RED, self.coord, self.op[-1])
            draw_at_mouse([self.coord, self.path], fill=True)
        draw_circle(self.coord, 5, True, self.co)
        if update:
            self.update(search_coords)


class CreateChar:
    def __init__(self, coord, obstacles, speed=2.5, colour=(0, 255, 0), size=5):
        self.coord = coord
        polys = []
        for _ in range(len(obstacles)):
            polys.append(Polygon(obstacles[_]))
        self.polys = polys
        self.speed = speed
        self.colour = colour
        self.s = size

    def update_polys(self, obstacles):
        polys = []
        for _ in range(len(obstacles)):
            polys.append(Polygon(obstacles[_]))
        self.polys = polys

    def update(self, x_adjust=0, y_adjust=0, draw=True):
        s = self.s
        x, y = self.coord
        x += x_adjust * self.speed
        y += y_adjust * self.speed
        b = Polygon(((x - s, y), (x, y + s), (x + s, y), (x, y - s)))
        intersect = False
        for _ in range(len(self.polys)):
            if self.polys[_].intersects(b):
                intersect = True
        if not intersect:
            self.coord = x, y
        if draw:
            draw_line(x, y, x + x_adjust * self.speed * 5, y + y_adjust * self.speed * 5, BLUE)
            draw_circle(self.coord, s, True, self.colour)


def create_line_segments(start, end, segment_length=10):
    x1, y1 = start
    x2, y2 = end

    # Calculate the distance between the points
    distance = ((x2 - x1)**2 + (y2 - y1)**2)**0.5

    # Determine the number of segments
    num_segments = int(distance / segment_length)

    # Calculate the step size for x and y coordinates
    try:
        x_step = (x2 - x1) / num_segments
    except ZeroDivisionError:
        x_step = 0
    try:
        y_step = (y2 - y1) / num_segments
    except ZeroDivisionError:
        y_step = 0

    # Create the segments
    segments = [(x1 + i * x_step, y1 + i * y_step) for i in range(num_segments + 1)]

    return segments


def run():
    obstacles = [
        [(0, 0), (screen_width, 0), (screen_width, screen_height), (0, screen_height)],
        [(400, 200), (600, 200), (600, 400), (400, 400)],
        [(100, 100), (150, 100), (150, 300), (100, 300)],
        [(100, 400), (150, 400), (150, 600), (100, 600)],
        [(200, 200), (210, 200), (210, 500), (200, 500)],
        [(100, 100), (screen_width, 100), (screen_width, 110), (100, 110)],
        [(100, 200), (150, 200), (150, 300), (100, 300)]
    ]
    col_obstacles = [
        [(400, 200), (600, 200), (600, 400), (400, 400)],
        [(0, 0), (screen_width, 0), (screen_width, 1), (0, 1)],
        [(0, 0), (screen_width, 0), (screen_width, 2), (0, 2)],
        [(0, 0), (2, 0), (2, screen_height), (0, screen_height)],
        [(screen_width, 0), (screen_width - 2, 0), (screen_width - 2, screen_height),
         (screen_width, screen_height)],
        [(0, screen_height - 2), (0, screen_height), (screen_width, screen_height),
         (screen_width, screen_height - 2)],
        [(100, 100), (150, 100), (150, 300), (100, 300)],
        [(100, 400), (150, 400), (150, 600), (100, 600)],
        [(200, 200), (210, 200), (210, 500), (200, 500)],
        [(100, 100), (screen_width, 100), (screen_width, 110), (100, 110)],
        [(100, 200), (150, 200), (150, 300), (100, 300)]
    ]
    d = 7
    ai_obstacles = []
    for obstacle in col_obstacles:
        obstacle = (obstacle[0][0] - d, obstacle[0][1] - d), (obstacle[1][0] + d, obstacle[1][1] - d), \
            (obstacle[2][0] + d, obstacle[2][1] + d), \
            (obstacle[3][0] - d, obstacle[3][1] + d)
        ai_obstacles.append(obstacle)
    k = 100
    ai_obstacles.append([(100, 200 + k), (150, 200 + k), (150, 300 + k), (100, 300 + k)])
    tim = CreateAI((screen_width - 20, screen_height - 20), ai_obstacles, ai='static')
    tom = CreateChar((10, 10), col_obstacles)
    running = True
    intersecting = False
    k = 0
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
        screen.fill(WHITE)
        # Draw obstacles
        obstacles[6] = [(100, 200 + k), (150, 200 + k), (150, 300 + k), (100, 300 + k)]
        col_obstacles[10] = [(100, 200 + k), (150, 200 + k), (150, 300 + k), (100, 300 + k)]
        tom.update_polys(col_obstacles)
        if not intersecting:
            k -= (k - 100) / 5
        else:
            k -= k / 50
        c = 0
        d = 5
        for obstacle in obstacles:
            obstacle = (obstacle[0][0] - d, obstacle[0][1] - d), (obstacle[1][0] + d, obstacle[1][1] - d), \
                (obstacle[2][0] + d, obstacle[2][1] + d), \
                (obstacle[3][0] - d, obstacle[3][1] + d)
            if c > 0:
                draw_poly_g(obstacle, True, GREY)
            c += 1
        draw_button(screen_width / 3, screen_height / 2, 'Press R to reset', fill=True)
        if keyboard.is_pressed('r'):
            tom.coord = [10, 10]
        # Draw shadows and chars
        draw_shadows(screen, obstacles, [tim.coord, tom.coord])
        intersecting = False
        if Polygon([(0, 300), (210, 300), (210, 400), (0, 400)]).contains(Point(tom.coord)):
            intersecting = True
        tim.path_find_to(get_mouse())
        tim.draw()
        x, y = 0, 0
        if keyboard.is_pressed('w'):
            y = -1
        elif keyboard.is_pressed('s'):
            y = 1
        if keyboard.is_pressed('a'):
            x = -1
        elif keyboard.is_pressed('d'):
            x = 1
        tom.update(x, y)
        # draw overlay

        edge = [
            [(0, 0), (screen_width, 0), (screen_width, 2), (0, 2)],
            [(0, 0), (2, 0), (2, screen_height), (0, screen_height)],
            [(screen_width, 0), (screen_width - 2, 0), (screen_width - 2, screen_height),
             (screen_width, screen_height)],
            [(0, screen_height - 2), (0, screen_height), (screen_width, screen_height),
             (screen_width, screen_height - 2)]
        ]
        for _ in range(4):
            draw_poly_g(edge[_], True)
        draw_button(7, screen_height - 27, f'{round(clock.get_fps())} FPS', fill=True, bg_colour=BG)
        clock.tick(60)
        pygame.display.flip()


run()
pygame.quit()
