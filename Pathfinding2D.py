import heapq
from time import time
from shapely.geometry import Polygon, Point
from shapely.affinity import scale
from math import sqrt


class Node:
    def __init__(self, x, y, cost=float('inf'), parent=None):
        self.x = x
        self.y = y
        self.cost = cost
        self.parent = parent
        self.heuristic = 0

    def __lt__(self, other):
        return self.cost + self.heuristic < other.cost + other.heuristic


def get_neighbors(node, grid):
    x, y = node.x, node.y
    neighbors = []
    for dx, dy in [(0, 1), (1, 0), (0, -1), (-1, 0), (-1, -1), (-1, 1), (1, 1), (1, -1)]:
        nx, ny = x + dx, y + dy
        if 0 <= nx < len(grid[0]) and 0 <= ny < len(grid) and grid[ny][nx] != 1:
            neighbors.append(Node(nx, ny))
    return neighbors


def euclidean_distance(a, b):
    return sqrt((a[0] - b[0])**2 + (a[1] - b[1])**2)


def a_star_search(grid, start, end, thresh=0.15):
    start_node = Node(*start, cost=0)
    end_node = Node(*end)
    s = time()
    open_list = []
    heapq.heappush(open_list, start_node)
    closed_list = set()

    while open_list:
        current_node = heapq.heappop(open_list)
        closed_list.add((current_node.x, current_node.y))

        if (current_node.x, current_node.y) == (end_node.x, end_node.y):
            path = []
            while current_node.parent:
                path.append((current_node.x, current_node.y))
                current_node = current_node.parent
            return path[::-1]

        neighbors = get_neighbors(current_node, grid)
        for neighbor in neighbors:
            if (neighbor.x, neighbor.y) in closed_list:
                continue

            tentative_cost = current_node.cost + 1

            if tentative_cost < neighbor.cost:
                neighbor.parent = current_node
                neighbor.cost = tentative_cost
                neighbor.heuristic = euclidean_distance((neighbor.x, neighbor.y), (end_node.x, end_node.y))

                heapq.heappush(open_list, neighbor)
        if time() - s > thresh:
            return [start]
    return []


def create_grid(screen_width, screen_height, obstacles, grid_size=1):
    grid = []
    grid_height = screen_height // grid_size
    grid_width = screen_width // grid_size

    for y in range(grid_height):
        row = []
        for x in range(grid_width):
            point = Point(x * grid_size, y * grid_size)
            if any(polygon.contains(point) for polygon in obstacles):
                row.append(1)
            else:
                row.append(0)
        grid.append(row)
    return grid


def scale_obstacles(obstacles, grid_size):
    scaled_obstacles = []
    for obstacle in obstacles:
        scaled_obstacle = scale(obstacle, 1/grid_size, 1/grid_size, origin=(0, 0))
        scaled_obstacles.append(scaled_obstacle)
    return scaled_obstacles


class CreatePathObject:
    def __init__(self, obstacles, screen_width, screen_height, thresh=0.15, grid_size=1):
        self.thresh = thresh
        polys = []
        for poly in obstacles:
            polys.append(Polygon(poly))
        polys = scale_obstacles(polys, grid_size)
        self.grid = create_grid(screen_width, screen_height, polys, grid_size)
        self.grid_size = grid_size

    def calculate(self, start, end):
        start = (round(start[0]), round(start[1]))
        end = (round(end[0]), round(end[1]))
        path = a_star_search(self.grid, start, end, self.thresh)
        return scale_path(path, self.grid_size)


def scale_path(path, grid_size):
    scaled_path = [(x * grid_size, y * grid_size) for x, y in path]
    return scaled_path
